import  service  from "@/utils/request.js"

/** 天行数据接口
 * 地址:https://www.tianapi.com/
 * key:7f8d5c765e8894db138156dc0de69c5e  
 */

/**
 * 获取国内疫情数据
 * @param params
 * { key: '', date: '' }
 */
 export function getEpidemicData(params) {
	return service({
		url: 'http://api.tianapi.com/ncov/index',
		method: 'get',
		params: {...params, key:'7f8d5c765e8894db138156dc0de69c5e'}
	})
}