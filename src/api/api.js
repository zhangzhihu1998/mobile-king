import service  from '../utils/request.js'

/**
 * 获取英雄列表
 * @param params
 * 
 */
export function pageHeroData(params) {
	return service({
		url: '/wzry/hero/list',
		method: 'get',
		params: params
	})
}

/**
 * 获取单个英雄数据
 * @param param 
 * @key hero_id
 */
export function getHeroDetail(params) {
	return service({
		url: `/wzry/hero/detail`,
		method: 'get',
		params: params
	})
}

/**
 * 获取装备列表
 * @param param 
 * 
 */
export function pageEquipmentData(params) {
	return service({
		url: '/wzry/equip/list',
		method: 'get',
		params: params
	})
}