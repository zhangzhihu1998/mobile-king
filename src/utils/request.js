import axios from 'axios'
// import store from '../store/index'

const service = axios.create({
  baseURL: "/king-api",  
  timeout: 10000,  
});

// 添加一个请求拦截器
service.interceptors.request.use(config => {  
    // config.headers["X-TOKEN"] = getToken("X-TOKEN")  // 让每个请求携带自定义token 请根据实际情况自行修改
    return config
  }, (error) => {
    return Promise.reject(error)
  }
)

// 添加一个响应拦截器
service.interceptors.response.use(response => {  
    return response
  }, (error) => {
    return Promise.reject(error)
  }
)

export default service
