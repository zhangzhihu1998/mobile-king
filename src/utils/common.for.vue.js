import { Toast } from "vant";
import moment from "moment";

export default {
  /**
   * 判断返回的状态码是否是200
   * @param {*} res 返回的参数
   * @param {*} successMsg 成功提示语
   * @param {*} successCallBack 是200的回调函数
   * @param {*} errorCallBack 失败的回调函数
   */
  CheckCode(res, successMsg, successCallBack, errorCallBack) {
    if (res) {
      if (res.code == 200) {
        if (successMsg) {
          Toast.success(successMsg);
        }
        successCallBack();
      } else if (res.code == 401) {
        // es
      } else {
        if (res.msg === "没有权限请先登陆") {
          // window.location.reload()
        } else {
          res.msg &&
            Toast({
              mes: res.msg,
              type: "error",
            });
        }
        errorCallBack && errorCallBack();
      }
    } else {
      errorCallBack && errorCallBack();
    }
  },

  /**
   * 格式化时间方法
   * @param {*} times 时间戳
   * @param {*} type 格式可选date, datetime;或自定义es:YYYY-MM-DD HH:ss:mm
   */
  formatDateTime(times, type) {
    let time = ''
    if(type === 'date') {
      time = moment(times).format('YYYY-MM-DD')
    } else if (type === 'datetime') {
      time = moment(times).format('YYYY-MM-DD HH:mm:ss')
    } else {
      time = moment(times).format(type)
    }
    return time
  }







  
}