 const tongxinPosition = {
  path: "/tongxinPosition",
  name: "tongxinPosition",
  component: () => import("@/views/tongxin-position/index.vue"),
  children: [
    {
      path: 'home',
      component: () => import('@/views/tongxin-position/home/Home.vue'),
      name: 'home',
      meta: {
        title: '桐心阵地'
      }
    },
    {
      path: "partyBuildingSystem",
      name: "partyBuildingSystem",
      component: () => import("@/views/tongxin-position/party-building-system/index.vue"),
      meta: {
        title: '党建体系'
      }
    },
    {
      path: "partyBuildingInstitution",
      name: "partyBuildingInstitution",
      component: () => import("@/views/tongxin-position/party-building-institution/index.vue"),
      meta: {
        title: '党建制度'
      }
    },
    {
      path: "partyBuildingAssessment",
      name: "partyBuildingAssessment",
      component: () => import("@/views/tongxin-position/party-building-assessment/index.vue"),
      meta: {
        title: '党建考核'
      }
    },
    {
      path: "partyBuildingAssessmentDetail",
      name: "partyBuildingAssessmentDetail",
      component: () => import("@/views/tongxin-position/party-building-assessment/detail.vue"),
      meta: {
        title: '党建考核详情'
      }
    },
    {
      path: "partyBuildingPosition",
      name: "partyBuildingPosition",
      component: () => import("@/views/tongxin-position/party-building-position/index.vue"),
      meta: {
        title: '党建阵地'
      }
    },
    {
      path: "partyBuildingPositionDetail",
      name: "partyBuildingPositionDetail",
      component: () => import("@/views/tongxin-position/party-building-position/detail.vue"),
      meta: {
        title: '党建阵地详情'
      }
    },
    {
      path: "partyHistoryStudy",
      name: "partyHistoryStudy",
      component: () => import("@/views/tongxin-position/party-history-study/index.vue"),
      meta: {
        title: '党史学习'
      }
    },
    {
      path: "partyHistoryStudyDetail",
      name: "partyHistoryStudyDetail",
      component: () => import("@/views/tongxin-position/party-history-study/detail.vue"),
      meta: {
        title: '党史学习活动详情'
      }
    },
    {
      path: "activityStyle",
      name: "activityStyle",
      component: () => import("@/views/tongxin-position/activity-style/index.vue"),
      meta: {
        title: '活动风采'
      }
    },
    {
      path: "activityStyleDetail",
      name: "activityStyleDetail",
      component: () => import("@/views/tongxin-position/activity-style/detail.vue"),
      meta: {
        title: '活动风采详情'
      }
    },
  ] 
}

export default tongxinPosition