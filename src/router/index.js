import Vue from 'vue'
import VueRouter from 'vue-router'
import tongxinPositionRouter from './modules/tongjianghui/tongxin-position.router.js'
import heroDataRouter from "./modules/gameData/heroData.router.js"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: "index",
    redirect:'/home',
    component: () => import('@/views/index.vue'),
    children: [
      {
        path: 'home',
        name: "home",
        component: () => import('@/views/Home/index.vue'),
      },
      {
        path: 'riskAreaDetail',
        name: "riskAreaDetail",
        component: () => import('@/views/Home/riskAreaDetail.vue'),  
      },
      {
        path: 'news',
        name: "news",
        component: () => import('@/views/news/index.vue'),
      },
      {
        path: 'newsDetail',
        name: "newsDetail",
        component: () => import('@/views/news/newsDetail/index.vue'),
      },
      {
        path: 'gameData',
        name: "gameData",
        component: () => import('@/views/gameData/index.vue'),
      },
      {
        path: 'my',
        name: "my",
        component: () => import('@/views/my/index.vue'),
      },
    ]
  },
  
  tongxinPositionRouter,
  heroDataRouter,
  {
    path: '/',
    name: "404",
    component: () => import('@/views/Home/index.vue'),
  },
]

const router = new VueRouter({
  routes
})

export default router
