import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant';
import 'vant/lib/index.css';
import '@/style/common.css'
import '@/style/vant-common.less'
import "lib-flexible"
// 公共方法
import common from '../src/utils/common.for.vue.js'
// 导入公共组件
import ComTitle from '@/components/ComTitle.vue'
import ComTreeChart from "@/components/ComTreeChart.vue"
import ComTabbar from "@/components/ComTabbar.vue"
import ComScroll from "@/components/ComScroll.vue"

Vue.config.productionTip = false
Vue.prototype.common = common
// 引用公共组件
Vue.component('ComTitle', ComTitle)
Vue.component('ComTreeChart', ComTreeChart)
Vue.component('ComTabbar', ComTabbar)
Vue.component('ComScroll', ComScroll)
Vue.use(Vant)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
