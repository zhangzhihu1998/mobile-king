
module.exports = {
  // 基本路径
  publicPath: "./",
  // 输出文件目录
  outputDir: "dist",

  // 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
  // assetsDir: '',

  //指定生成的 index.html 的输出路径 (相对于 outputDir)。也可以是一个绝对路径。
  indexPath: "index.html",

  // 是否使用包含运行时编译器的 Vue 构建版本。设置为 true 后你就可以在 Vue 组件中使用 template 选项了，但是这会让你的应用额外增加 10kb 左右。
  runtimeCompiler: true,

  // 默认情况下 babel-loader 会忽略所有 node_modules 中的文件。如果你想要通过 Babel 显式转译一个依赖，可以在这个选项中列出来。
  transpileDependencies: [],

  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
  productionSourceMap: false,

  // eslint-loader 是否在保存的时候检查
  lintOnSave: true,

  configureWebpack: {
    //警告 webpack 的性能提示
    performance: {
      hints: false,
      //入口起点的最大体积
      maxEntrypointSize: 50000000,
      //生成文件的最大体积
      maxAssetSize: 30000000,
      //只给出 js 文件的性能提示
      assetFilter: function (assetFilename) {
        return assetFilename.endsWith(".js");
      }
    }
  },
  // vue-loader 配置项

  // css相关配置
  css: {
    // 是否使用css分离插件 ExtractTextPlugin
    extract: false,
    // 开启 CSS source maps?
    sourceMap: false,
    loaderOptions: {
      css: {},
      postcss: {
        plugins: [
          require('postcss-px2rem')({
            rootValue: 75,
              // remUnit: 16
          })
        ]
      }
    }
  },
  // css: {
  //   // 是否使用css分离插件 ExtractTextPlugin
  //   extract: false,
  //   // 开启 CSS source maps?
  //   sourceMap: false,

  //   // 启用 CSS modules for all css / pre-processor files.
  //   modules: false
  // },

  // 是否为 Babel 或 TypeScript 使用 thread-loader。该选项在系统的 CPU 有多于一个内核时自动启用，仅作用于生产构建。
  parallel: require("os").cpus().length > 1,

  // PWA 插件相关配置
  // see https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
  pwa: {},

  // webpack-dev-server 相关配置
  devServer: {
    host: "0.0.0.0",
    port: 8080,
    https: false,
    hotOnly: false,
    open: true, // 自动打开浏览器
    proxy: {
      "/king-api": {
        target: "http://gamehelper.gm825.com",
        changeOrigin: true,
        secure: false,
        pathRewrite: {
					"^/king-api": ""
				}
      },
      "/api": {
        target: "http://39.104.92.226:80",
        changeOrigin: true,
        secure: false,
        pathRewrite: {
					"^api": ""
				}
      },
    }
  },
  // 第三方插件配置
  pluginOptions: {
    // ...
    "plugins": {
      'postcss-pxtorem': {
        rootValue: 75,
        // rootValue({ file }) {
        //   return file.indexOf('vant') !== -1 ? 37.5 : 75;
        // },
        propList: ['*'],
      },
    }
  }
};
